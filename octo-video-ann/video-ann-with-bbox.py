"""
Based on https://github.com/seoungwugoh/ivs-demo

The entry point for the user interface
It is terribly long... GUI code is hard to write!
"""

import sys
import os
from os import path
import functools
from argparse import ArgumentParser

import cv2
from PIL import Image
import numpy as np
import torch
from collections import deque

from PyQt5.QtWidgets import (QMainWindow, QWidget,      QApplication, QComboBox,
                             QHBoxLayout, QLabel, QPushButton, QTextEdit,
                             QPlainTextEdit, QVBoxLayout, QSizePolicy, QButtonGroup, QSlider,
                             QShortcut, QRadioButton, QProgressBar, QFileDialog)

from PyQt5.QtGui import QPixmap, QKeySequence, QImage, QTextCursor
from PyQt5.QtCore import Qt, QTimer
from PyQt5 import QtCore
from PyQt5 import QtCore, QtGui, QtWidgets

# from utils.canvas import Canvas
# from utils.canvas import CanvasRect as Canvas


class CVMediaPlayer(QtWidgets.QLabel):
    def __init__(self):
        super().__init__()
        self.state = None

    def play(self):
        pass

    def pause(self):
        pass

    def setPosition(self, pos):
        pass

    def position(self):
        pass

    def setMedia(self):
        pass

    def get_state(self):
        pass

    def set_state(self, cmd):
        self.state = cmd


class VideoWidget():
    pass


class VideoPlayer(QWidget):

    def __init__(self, parent=None):
        global gantChart
        global Topics

        super(VideoPlayer, self).__init__(parent)
        self.mediaPlayer = CVMediaPlayer()

        Topics = None
        self.time_ = 0
        self.time_dif = 0
        self.duration = 0
        self.message_count = 0
        self.videobox = []
        self.box_buffer = []
        self.metric_buffer = []

        # >> DEFINE WIDGETS OCJECTS
        # >> VIDEO - AUDIO - GANTT CHART
        # ----------------------
        self.videoWidget = VideoWidget()
        self.videoWidget.setFixedSize(640, 480)

        # Video buttons
        videoLayout = self.createVideoButtons()

        # Create Slider
        self.createSlider()

        self.controlEnabled = False

        # Specify video layout align
        laserAndVideoLayout = QHBoxLayout()
        laserAndVideoLayout.addLayout(videoLayout)

        # # Audio Player buttons
        # buttonLayoutAudio = self.createAudioButtons()
        # waveLayout = self.createAudio()

        self.mainLayout = QVBoxLayout()
        self.mainLayout.addLayout(laserAndVideoLayout)
        self.mainLayout.addWidget(self.positionSlider)
        self.setLayout(self.mainLayout)

        self.mediaPlayer.setVideoOutput(self.videoWidget.videoSurface())
        self.mediaPlayer.stateChanged.connect(self.mediaStateChanged)
        self.mediaPlayer.positionChanged.connect(self.positionChanged)
        self.mediaPlayer.durationChanged.connect(self.durationChanged)

        self.video_extensions = ['.mp4', '.avi']

    def createSlider(self):
        self.positionSlider = QSlider(Qt.Horizontal)
        self.positionSlider.setMinimum(0)
        self.positionSlider.setMaximum(self.duration)
        self.positionSlider.setTickInterval(1)
        self.positionSlider.sliderMoved.connect(self.setPosition)

    def createVideoButtons(self):

        self.playButton = QPushButton()
        self.playButton.setEnabled(False)
        self.playButton.setShortcut(QKeySequence(Qt.Key_Space))
        self.playButton.setIcon(self.style().standardIcon(
            QtWidgets.QStyle.SP_MediaPlay))
        self.playButton.clicked.connect(self.play)

        self.previousButton = QPushButton()
        self.previousButton.setIcon(
            self.style().standardIcon(QtWidgets.QStyle.SP_MediaSeekBackward))
        self.previousButton.setShortcut(QKeySequence(Qt.ALT + Qt.Key_A))
        self.previousButton.clicked.connect(self.previousFrame)

        self.nextButton = QPushButton()
        self.nextButton.setIcon(
            self.style().standardIcon(QtWidgets.QStyle.SP_MediaSeekForward))
        self.nextButton.setShortcut(QKeySequence(Qt.ALT + Qt.Key_D))
        self.nextButton.clicked.connect(self.nextFrame)

        # add label to slider about elapsed time
        self.label_tmp = '<b><FONT SIZE=3>{}</b>'
        self.timelabel = QLabel(self.label_tmp.format(
            'Time: ' + str(self.duration)))

        self.label = QHBoxLayout()
        self.label.addWidget(self.timelabel)
        self.label.setAlignment(Qt.AlignRight)

        self.controlLayout = QHBoxLayout()
        self.controlLayout.addWidget(self.playButton)
        self.controlLayout.addWidget(self.previousButton)
        self.controlLayout.addWidget(self.nextButton)
        self.controlLayout.setAlignment(Qt.AlignLeft)

        self.newLayout = QHBoxLayout()
        self.newLayout.addLayout(self.controlLayout)
        self.newLayout.addLayout(self.label)

        videoLayout = QVBoxLayout()
        videoLayout.addWidget(self.videoWidget)
        videoLayout.addLayout(self.newLayout)

        return videoLayout

    def pauseMedia(self):
        self.mediaPlayer.pause()
        self.Pause()

    def previousFrame(self):
        global frameCounter
        if frameCounter > 0:
            frameCounter -= 2
            pos = round(
                ((frameCounter) * (self.duration * 1000)) / self.message_count)
            self.mediaPlayer.setPosition(pos)

    def nextFrame(self):
        global frameCounter

        if frameCounter < self.message_count:
            pos = round(
                ((frameCounter) * (self.duration * 1000)) / self.message_count)
            self.mediaPlayer.setPosition(pos)

    def videoPosition(self):
        self.videoTime = self.mediaPlayer.position()

    def openFile(self):
        global framerate
        # global bagFile
        global rgbFileName
        global Topics
        # global audio_player
        framerate = 0

        fileName, _ = QFileDialog.getOpenFileName(
            self, "Open Movie", QtCore.QDir.homePath())
        # create a messsage box for get or load data info
        if fileName:
            name, extension = os.path.splitext(fileName)
            self.videobox = []
            if extension in self.video_extensions:
                video_player = True
                self.duration, framerate, self.message_count = rosbagRGB.get_metadata(
                    fileName)
                self.videobox = [boundBox(count/framerate)
                                 for count in range(int(self.message_count))]
                self.mediaPlayer.setMedia(fileName)
                self.playButton.setEnabled(True)
                rgbFileName = fileName

            # mainWindow.repaint(player.videobox, framerate)
            mainWindow.setWindowTitle(fileName)
            self.setWindowTitle(fileName + ' -> Annotation')

    def play(self):
        global frameCounter
        # global audio_player
        global video_player
        if self.mediaPlayer.state() == 'playing':
            self.videoPosition()
            self.mediaPlayer.pause()
            # if audio_player:
            #     self.audioPlay()
            self.time_ = self.positionSlider
        else:
            self.time_ = self.mediaPlayer.position()
            # if audio_player:
            #     self.player.setPosition(self.time_)
            #     self.end = audioGlobals.duration*1000 - 10
            #     self.audioPlay()
            self.mediaPlayer.play()

        # >> Get slider position for bound box
        posSlider = self.positionSlider.value()
        # self.tickLabel.setAlignment(posSlider)
        frameCounter = int(
            round((self.message_count * posSlider)/(self.duration * 1000)))

    def mediaStateChanged(self, state):
        if state == QMediaPlayer.PlayingState:
            self.playButton.setIcon(
                self.style().standardIcon(QtWidgets.QStyle.SP_MediaPause))
        else:
            self.playButton.setIcon(
                self.style().standardIcon(QtWidgets.QStyle.SP_MediaPlay))

    def positionChanged(self, position):
        time = "{0:.2f}".format(float(position)/1000)
        self.positionSlider.setValue(position)
        self.positionSlider.setToolTip(str(time) + ' sec')
        self.timelabel.setText(self.label_tmp.format(
            'Time: ' + str(time) + '/ ' + str("{0:.2f}".format(self.duration)) + ' sec'))

        # if audioGlobals.duration > 0 and self.mediaPlayer.state() != 0:
        #     audioGlobals.fig.drawNew(time)
        #     audioGlobals.fig.draw()

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Control:
            self.controlEnabled = True

    def keyReleaseEvent(self, event):
        if event.key() == Qt.Key_Control:
            self.controlEnabled = False

    def durationChanged(self, duration):
        self.positionSlider.setRange(0, duration)

    def setPosition(self, position):
        global frameCounter
        # global audio_player
        global video_player

        frameCounter = int(
            round(self.message_count * position/(self.duration * 1000)))
        if frameCounter >= self.message_count:
            frameCounter = self.message_count - 1
        self.mediaPlayer.setPosition(position)
        # if audio_player:
        #     self.player.setPosition(position)


# Holds the bound box parameters


class boundBox():

    def __init__(self, time):
        self.timestamp = time
        self.box_id = []
        self.box_Param = []
        self.features = []
        self.annotation = []

    def addBox(self, time, box_id, params, classify, features):
        self.timestamp = time

        boxNumber = -1
        if box_id is None:
            # If id already in the list then give the next id
            for i in range(len(self.box_id)):
                if(i != self.box_id[i]):
                    boxNumber = i
            if boxNumber == -1:
                boxNumber = len(player.videobox[frameCounter].box_id)
        else:
            boxNumber = box_id
        self.box_id.append(boxNumber)
        self.box_Param.append(params[::])
        self.annotation.append(classify)
        self.features.append(features)

    def removeAllBox(self):
        self.box_id[:] = []
        self.features[:] = []
        self.box_Param[:] = []
        self.annotation[:] = []

    def removeSpecBox(self, index):
        self.box_id.pop(index)
        self.features.pop(index)
        self.box_Param.pop(index)
        self.annotation.pop(index)

    # Handles the annotation for basic and high level classes
    def changeClass(self, boxid, classify):
        if boxid in self.box_id:
            if classify in videoGlobals.classLabels:
                self.annotation[boxid][0] = classify
            elif classify not in self.annotation[self.box_id.index(boxid)]:
                self.annotation[self.box_id.index(boxid)].append(classify)

    # Remove high level events
    def removeEvent(self, boxid, action):
        if boxid in self.box_id:
            # boxid is the index of boxes
            for key in self.annotation[self.box_id.index(boxid)]:
                if action == key:
                    self.annotation[self.box_id.index(boxid)].remove(key)

    def copy(self, other):
        self.box_id = []
        self.features = []
        self.box_Param = []
        self.annotation = []

        for i in other.box_id:
            self.box_id.append(i)
        for i in other.box_Param:
            self.box_Param.append(i)
        for i in other.annotation:
            self.annotation.append(i)
        for i in other.features:
            self.features.append(i)


class MainWindow(QMainWindow):

    def __init__(self, player):
        super(MainWindow, self).__init__()
        self.setCentralWidget(player)
        self.createActions()
        self.fileMenu = self.menuBar().addMenu("&File")
        self.fileMenu.addAction(self.openBagAct)
        self.fileMenu.addAction(self.openCsvAct)
        self.fileMenu.addAction(self.saveCsvAct)
        self.fileMenu.addAction(self.quitAct)

        self.editMenu = self.menuBar().addMenu("&Video")
        self.editMenu.addAction(self.editLabelsAct)
        self.editMenu.addAction(self.deleteAct)
        self.editMenu.addAction(self.copyAct)
        self.editMenu.addAction(self.shotcutAct)

    def createActions(self):
        self.openBagAct = QAction("&Open video/audio/rosbag", self, shortcut="Ctrl+B",
                                  statusTip="Open video/audio/rosbag", triggered=self.open)
        self.openCsvAct = QAction("&Open video csv", self, shortcut="Ctrl+V",
                                  statusTip="Open csv", triggered=self.openCSV)
        self.saveCsvAct = QAction("&Save audio/video csv", self, shortcut="Ctrl+S",
                                  statusTip="Save csv", triggered=self.saveCSV)
        self.quitAct = QAction("&Quit", self, shortcut="Ctrl+Q",
                               statusTip="Quit", triggered=self.closeEvent)

        self.editLabelsAct = QAction("Edit Video Labels", self,
                                     statusTip="Edit Labels", triggered=self.edit_labels)
        self.deleteAct = QAction("Delete All Boxes", self, shortcut=Qt.ALT + Qt.Key_R,
                                 statusTip="Delete All Boxes", triggered=self.deleteEvent)
        self.copyAct = QAction("Copy Latest Boxes", self, shortcut=Qt.ALT + Qt.Key_E,
                               statusTip="Copy Latest Boxes", triggered=self.copyPrevious)

        self.shotcutAct = QAction("Shortcuts", self, statusTip="Shortcut information",
                                  triggered=self.shortcuts)

    def open(self):
        player.openFile()

    def openCSV(self):
        player.openCsv()

    def saveCSV(self):
        player.writeCSV()
        eA.writeCSV()

    def close(self):
        sys.exit(app)

    def saveAndClose(self):
        player.writeCSV()
        eA.writeCSV()
        sys.exit(app)

    def closeEvent(self, event):
        global bagFile
        global videoCSV
        if bagFile and videoCSV:
            msgBox = QMessageBox()
            msgBox.setIcon(msgBox.Warning)
            msgBox.setWindowTitle("Quit")
            msgBox.setText("<b><font size=\"5\">You may have unfinished work")
            msgBox.setInformativeText("Do you want to save before quitting?")
            yesButton = QPushButton()
            yesButton.setText("Yes")
            yesButton.clicked.connect(self.saveAndClose)
            msgBox.addButton(yesButton, QMessageBox.YesRole)

            noButton = QPushButton()
            noButton.setText("No")
            noButton.clicked.connect(self.close)
            msgBox.addButton(noButton, QMessageBox.NoRole)

            cancelButton = QPushButton()
            cancelButton.setText("Cancel")
            msgBox.addButton(cancelButton, QMessageBox.RejectRole)
            retval = msgBox.exec_()
            if retval == 2 and type(event) != bool:
                event.ignore()
        else:
            self.close()

    def deleteEvent(self, event):
        global rgbFileName
        global videoCSV
        if rgbFileName:
            player.videobox[frameCounter].removeAllBox()
            self.repaint(player.videobox, framerate)

    def copyPrevious(self):
        if frameCounter > 0:
            for i in range(frameCounter - 1, 0, -1):
                box = player.videobox[i]
                if box.box_id:
                    for j in range(i + 1, frameCounter + 1):
                        player.videobox[j].copy(box)
                    break
            self.repaint(player.videobox, framerate)

    def shortcuts(self):
        pass
        # self.shortcuts = videoShortcuts.videoShortCuts()
        # self.shortcuts.show()

    def edit_labels(self):
        pass
        # self.editLabels = editLabels.editLabels()
        # self.editLabels.show()

    def repaint(self, videobox, framerate):
        player.videoWidget.repaint()


if __name__ == '__main__':
    os.system('cls' if os.name == 'nt' else 'clear')
    app = QApplication(sys.argv)

    player = VideoPlayer()
    mainWindow = MainWindow(player)
    mainWindow.show()

    app.exec_()
    # try:
    #     csvFileName = audioGlobals.bagFile.replace(".bag", "_audio.csv")
    #     if audioGlobals.saveAudio == False:
    #         saveAudioSegments.save(csvFileName, audioGlobals.wavFileName)
    # except:
    #     pass
