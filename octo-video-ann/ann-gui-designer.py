# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ann-gui-designer.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING! All changes made in this file will be lost!

import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(70, 80, 361, 321))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.videoplayer = QtWidgets.QWidget(self.verticalLayoutWidget)
        self.videoplayer.setObjectName("videoplayer")
        self.verticalLayout.addWidget(self.videoplayer)
        self.scrollArea = QtWidgets.QScrollArea(self.centralwidget)
        self.scrollArea.setGeometry(QtCore.QRect(450, 80, 241, 321))
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 239, 319))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.formLayoutWidget = QtWidgets.QWidget(
            self.scrollAreaWidgetContents)
        self.formLayoutWidget.setGeometry(QtCore.QRect(10, 10, 160, 80))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout = QtWidgets.QFormLayout(self.formLayoutWidget)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.personNameLabel = QtWidgets.QLabel(self.formLayoutWidget)
        self.personNameLabel.setObjectName("personNameLabel")
        self.formLayout.setWidget(
            0, QtWidgets.QFormLayout.LabelRole, self.personNameLabel)
        self.personNameLineEdit = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.personNameLineEdit.setObjectName("personNameLineEdit")
        self.formLayout.setWidget(
            0, QtWidgets.QFormLayout.FieldRole, self.personNameLineEdit)
        self.categoryCheckBox = QtWidgets.QCheckBox(self.formLayoutWidget)
        self.categoryCheckBox.setObjectName("categoryCheckBox")
        self.formLayout.setWidget(
            1, QtWidgets.QFormLayout.FieldRole, self.categoryCheckBox)
        self.categoryLabel = QtWidgets.QLabel(self.formLayoutWidget)
        self.categoryLabel.setObjectName("categoryLabel")
        self.formLayout.setWidget(
            1, QtWidgets.QFormLayout.LabelRole, self.categoryLabel)
        self.formLayoutWidget_2 = QtWidgets.QWidget(
            self.scrollAreaWidgetContents)
        self.formLayoutWidget_2.setGeometry(QtCore.QRect(10, 110, 160, 80))
        self.formLayoutWidget_2.setObjectName("formLayoutWidget_2")
        self.formLayout_2 = QtWidgets.QFormLayout(self.formLayoutWidget_2)
        self.formLayout_2.setContentsMargins(0, 0, 0, 0)
        self.formLayout_2.setObjectName("formLayout_2")
        self.personNameLabel_2 = QtWidgets.QLabel(self.formLayoutWidget_2)
        self.personNameLabel_2.setObjectName("personNameLabel_2")
        self.formLayout_2.setWidget(
            0, QtWidgets.QFormLayout.LabelRole, self.personNameLabel_2)
        self.personNameLineEdit_2 = QtWidgets.QLineEdit(
            self.formLayoutWidget_2)
        self.personNameLineEdit_2.setObjectName("personNameLineEdit_2")
        self.formLayout_2.setWidget(
            0, QtWidgets.QFormLayout.FieldRole, self.personNameLineEdit_2)
        self.categoryCheckBox_2 = QtWidgets.QCheckBox(self.formLayoutWidget_2)
        self.categoryCheckBox_2.setObjectName("categoryCheckBox_2")
        self.formLayout_2.setWidget(
            1, QtWidgets.QFormLayout.FieldRole, self.categoryCheckBox_2)
        self.categoryLabel_2 = QtWidgets.QLabel(self.formLayoutWidget_2)
        self.categoryLabel_2.setObjectName("categoryLabel_2")
        self.formLayout_2.setWidget(
            1, QtWidgets.QFormLayout.LabelRole, self.categoryLabel_2)
        self.formLayoutWidget_3 = QtWidgets.QWidget(
            self.scrollAreaWidgetContents)
        self.formLayoutWidget_3.setGeometry(QtCore.QRect(10, 210, 160, 80))
        self.formLayoutWidget_3.setObjectName("formLayoutWidget_3")
        self.formLayout_3 = QtWidgets.QFormLayout(self.formLayoutWidget_3)
        self.formLayout_3.setContentsMargins(0, 0, 0, 0)
        self.formLayout_3.setObjectName("formLayout_3")
        self.personNameLabel_3 = QtWidgets.QLabel(self.formLayoutWidget_3)
        self.personNameLabel_3.setObjectName("personNameLabel_3")
        self.formLayout_3.setWidget(
            0, QtWidgets.QFormLayout.LabelRole, self.personNameLabel_3)
        self.personNameLineEdit_3 = QtWidgets.QLineEdit(
            self.formLayoutWidget_3)
        self.personNameLineEdit_3.setObjectName("personNameLineEdit_3")
        self.formLayout_3.setWidget(
            0, QtWidgets.QFormLayout.FieldRole, self.personNameLineEdit_3)
        self.categoryCheckBox_3 = QtWidgets.QCheckBox(self.formLayoutWidget_3)
        self.categoryCheckBox_3.setObjectName("categoryCheckBox_3")
        self.formLayout_3.setWidget(
            1, QtWidgets.QFormLayout.FieldRole, self.categoryCheckBox_3)
        self.categoryLabel_3 = QtWidgets.QLabel(self.formLayoutWidget_3)
        self.categoryLabel_3.setObjectName("categoryLabel_3")
        self.formLayout_3.setWidget(
            1, QtWidgets.QFormLayout.LabelRole, self.categoryLabel_3)
        self.verticalScrollBar = QtWidgets.QScrollBar(
            self.scrollAreaWidgetContents)
        self.verticalScrollBar.setGeometry(QtCore.QRect(220, 0, 16, 311))
        self.verticalScrollBar.setOrientation(QtCore.Qt.Vertical)
        self.verticalScrollBar.setObjectName("verticalScrollBar")
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(460, 450, 113, 32))
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(580, 450, 113, 32))
        self.pushButton_2.setObjectName("pushButton_2")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 24))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.personNameLabel.setText(_translate("MainWindow", "PersonName"))
        self.categoryLabel.setText(_translate("MainWindow", "Category1"))
        self.personNameLabel_2.setText(_translate("MainWindow", "PersonName"))
        self.categoryLabel_2.setText(_translate("MainWindow", "Category1"))
        self.personNameLabel_3.setText(_translate("MainWindow", "PersonName"))
        self.categoryLabel_3.setText(_translate("MainWindow", "Category1"))
        self.pushButton.setText(_translate("MainWindow", "Import"))
        self.pushButton_2.setText(_translate("MainWindow", "Export"))


class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(ApplicationWindow, self).__init__()

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)


def main():
    app = QtWidgets.QApplication(sys.argv)
    application = ApplicationWindow()
    application.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
