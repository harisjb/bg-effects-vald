from PyQt5 import QtCore, QtGui, QtWidgets
from .bbox import BoundBox
from .ann import AnnLabels


class Canvas(QtWidgets.QLabel):
    def __init__(self, main_win):
        super().__init__()
        self.pix_w = 720
        self.pix_h = 420
        pixmap = QtGui.QPixmap(self.pix_w, self.pix_h)
        self.setPixmap(pixmap)
        self.begin, self.dest = QtCore.QPoint(), QtCore.QPoint()
        self.BoundBoxes = BoundBox()
        self.mainWin = main_win
        self.set_window_size()
        self.create_context_menu()

    def set_window_size(self):
        # width = 720
        # height = 420
        self.setFixedWidth(self.pix_w)
        self.setFixedHeight(self.pix_h)

    def set_pen_color(self, painter):
        p = painter.pen()
        p.setWidth(1)
        pen_color = QtGui.QColor("#FF0000")
        p.setColor(pen_color)
        painter.setPen(p)

    def getTopLeft(self, rect):
        x1, y1, x2, y2 = rect.getCoords()
        x = min(x1, x2)
        y = min(y1, y2)
        h = abs(y1 - y2)
        w = abs(x1 - x2)
        return x, y, h, w

    def mousePressEvent(self, event):
        if event.buttons() & QtCore.Qt.LeftButton:
            self.begin = event.pos()
            self.dest = self.begin
            self.update()

    def mouseMoveEvent(self, event):
        if event.buttons() & QtCore.Qt.LeftButton:
            self.dest = event.pos()
            self.update()

    def mouseReleaseEvent(self, event):
        if event.button() & QtCore.Qt.LeftButton:
            rect = QtCore.QRect(self.begin, self.dest)
            # x, y, w, h = rect.getRect()
            painter = QtGui.QPainter(self.pixmap())
            self.set_pen_color(painter)
            # print(rect.normalized())
            # painter.drawRect(rect.normalized())
            painter.drawRect(rect)
            boxid = self.BoundBoxes.addBox(rect)
            if boxid is not None:
                # add boxid
                disp_txt = QtWidgets.QTableWidgetItem(str(boxid))
                col_ind = AnnLabels.col_names[AnnLabels.boxid_lbl]
                self.mainWin.tableWidget.setItem(boxid, col_ind, disp_txt)
                # add start frame
                disp_txt = QtWidgets.QTableWidgetItem(
                    str(self.mainWin.getFPosition()))
                col_ind = AnnLabels.col_names[AnnLabels.stframe_lbl]
                self.mainWin.tableWidget.setItem(boxid, col_ind, disp_txt)
                # add metadata
                x1, y1, x2, y2 = rect.getCoords()

                x, y, h, w = self.getTopLeft(rect)
                metadata = {'box_xyhw': [x, y, h, w],
                            'canvas_wh': [self.pix_w, self.pix_h]}
                metadata = str(metadata)
                # disp_txt = QtWidgets.QTableWidgetItem(
                #     "{},{},{},{}".format(x1, y1, x2, y2))
                disp_txt = QtWidgets.QTableWidgetItem(metadata)
                col_ind = AnnLabels.col_names[AnnLabels.metadata_lbl]
                self.mainWin.tableWidget.setItem(boxid, col_ind, disp_txt)

                painter.setPen(QtGui.QColor(255, 255, 255))
                # x, y, w, h = self.getTopLeft(rect)
                painter.drawText(QtCore.QRectF(
                    x+2, y, h, w), QtCore.Qt.AlignLeft, str(boxid))
                # painter.drawText(x, y,
                #                  QtCore.Qt.AlignLeft, str(boxid))
            painter.end()
            self.begin, self.dest = QtCore.QPoint(), QtCore.QPoint()
            self.update()
            self.mainWin.refreshDisp()

    # def paintEvent(self, event):
    #     painter = QtGui.QPainter(self)
    #     for boxid, param in self.BoundBoxes.bboxes.items():
    #         rect = param
    #         painter.drawRect(rect.normalized)
    #     painter.end()
    #     # Shows the right click menu

    def create_context_menu(self):
        self.rc_menu = QtWidgets.QMenu(self)
        self.rc_menu.addSeparator()

        # Create issues menu
        self.issueMenuParent = self.rc_menu.addMenu('Add Issue')
        # self.issueSubMenuParents = {}
        self.issueMenuActionName = {}
        self.issueMenuActionFun = {}
        for submenu_name, issues_name in AnnLabels.issue_names.items():
            submenu_fun = self.issueMenuParent.addMenu(submenu_name)

            for issue_ii in issues_name:
                action_fun = submenu_fun.addAction(issue_ii)
                action_name = submenu_name + ';' + issue_ii
                if action_name in self.issueMenuActionFun:
                    raise ValueError('{} name is taken'.format(action_name))
                self.issueMenuActionFun[action_name] = action_fun
                self.issueMenuActionName[action_fun] = action_name

        # create end frame action
        self.endframe_action = self.rc_menu.addAction("Add End Frame")

        # Add comment action
        self.comments_action = self.rc_menu.addAction("Add Comments")

        # Add delete menu
        deleteMenuStruct = {'name': 'Delete',
                            'actions': ['Delete Box', 'Delete All Boxes']}
        self.deleteMenuParent = self.rc_menu.addMenu(deleteMenuStruct['name'])
        self.deleteMenuActionName = {}
        self.deleteMenuActionFun = {}
        for action_name in deleteMenuStruct['actions']:
            action_fun = self.deleteMenuParent.addAction(action_name)
            self.deleteMenuActionFun[action_name] = action_fun
            self.deleteMenuActionName[action_fun] = action_name

    def process_del_request(self, req_name, boxid):
        if req_name == 'Delete Box':
            self.BoundBoxes.removeSpecBox(boxid)
            self.mainWin.tableWidget.removeRow(boxid)
            self.mainWin.tableWidget.insertRow(boxid)
            self.mainWin.refreshDisp()

        elif req_name == 'Delete All Boxes':
            self.BoundBoxes.removeAllBox()
            self.mainWin.clearTable()
            self.mainWin.refreshDisp()
        else:
            raise ValueError("Invalid delete request: {}".format(req_name))

    def process_addissue_request(self, boxid, issue):
        if issue != "Other;Type Other Issue":
            col_ind = AnnLabels.col_names[AnnLabels.issue_lbl]
            disp_txt = QtWidgets.QTableWidgetItem(str(issue))
            self.mainWin.tableWidget.setItem(boxid, col_ind, disp_txt)
        else:
            col_ind = AnnLabels.col_names[AnnLabels.issue_lbl]
            self.mainWin.tableWidget.setCurrentCell(boxid, col_ind)

    def process_comment_request(self, boxid):
        col_ind = AnnLabels.col_names[AnnLabels.comments_lbl]
        self.mainWin.tableWidget.setCurrentCell(boxid, col_ind)

    def process_endframe_request(self, boxid):
        col_ind = AnnLabels.col_names[AnnLabels.endframe_lbl]
        end_frame = QtWidgets.QTableWidgetItem(
            str(self.mainWin.getFPosition()))
        self.mainWin.tableWidget.setItem(boxid, col_ind, end_frame)

    def contextMenuEvent(self, event):
        if len(self.BoundBoxes.bboxes) > 0:
            if event.reason() == QtGui.QContextMenuEvent.Mouse:
                posX = event.pos().x()
                posY = event.pos().y()

                selectedbox = None
                for boxid, ann in self.BoundBoxes.bboxes.items():
                    rect = ann[AnnLabels.metadata_lbl]
                    x1, y1, x2, y2 = rect.getCoords()
                    if posX > min(x1, x2) and posX < max(x1, x2) and posY > min(y1, y2) and posY < max(y1, y2):
                        selectedbox = boxid

                # If the mouse click is inside a box
                if selectedbox is not None:
                    self.context_menu = True
                    menu = self.rc_menu
                    action = menu.exec_(self.mapToGlobal(event.pos()))
                    # Check which submenu clicked
                    if action is not None:
                        if action == self.endframe_action:
                            self.process_endframe_request(selectedbox)
                        elif action.parent().parent() == self.issueMenuParent:
                            action_name = self.issueMenuActionName.get(action)
                            self.process_addissue_request(
                                selectedbox, action_name)
                        elif action == self.comments_action:
                            self.process_comment_request(selectedbox)
                        elif action.parent() == self.deleteMenuParent:
                            action_name = self.deleteMenuActionName.get(action)
                            self.process_del_request(action_name, selectedbox)
                        else:
                            raise ValueError('Unknown Context Menu Clicked')

                self.buttonLabels = []
                self.context_menu = False


class CanvasSimpleDraw(QtWidgets.QLabel):
    def __init__(self):
        super().__init__()
        pixmap = QtGui.QPixmap(720, 405)
        self.setPixmap(pixmap)
        self.last_x, self.last_y = None, None
        self.pen_color = QtGui.QColor("#000000")

    def set_pen_color(self, c):
        self.pen_color = QtGui.QColor(c)

    def mouseMoveEvent(self, e):
        if self.last_x is None:  # First event.
            self.last_x = e.x()
            self.last_y = e.y()
            return  # Ignore the first time.
        painter = QtGui.QPainter(self.pixmap())
        p = painter.pen()
        p.setWidth(4)
        p.setColor(self.pen_color)
        painter.setPen(p)
        painter.drawLine(self.last_x, self.last_y, e.x(), e.y())
        painter.end()
        self.update()
        # Update the origin for next time.
        self.last_x = e.x()
        self.last_y = e.y()

    def mouseReleaseEvent(self, e):
        self.last_x = None
        self.last_y = None


class CanvasSimpleRect(QtWidgets.QLabel):
    def __init__(self):
        super().__init__()
        pixmap = QtGui.QPixmap(720, 405)
        self.setPixmap(pixmap)
        self.begin, self.dest = QtCore.QPoint(), QtCore.QPoint()

    def set_pen_color(self, painter):
        p = painter.pen()
        p.setWidth(2)
        pen_color = QtGui.QColor("#FF0000")
        p.setColor(pen_color)
        painter.setPen(p)

    def mousePressEvent(self, event):
        if event.buttons() & QtCore.Qt.LeftButton:
            self.begin = event.pos()
            self.dest = self.begin
            self.update()

    def mouseMoveEvent(self, event):
        if event.buttons() & QtCore.Qt.LeftButton:
            self.dest = event.pos()
            self.update()

    def mouseReleaseEvent(self, event):
        if event.button() & QtCore.Qt.LeftButton:
            rect = QtCore.QRect(self.begin, self.dest)
            painter = QtGui.QPainter(self.pixmap())
            self.set_pen_color(painter)
            # print(rect.normalized())
            painter.drawRect(rect.normalized())

            self.begin, self.dest = QtCore.QPoint(), QtCore.QPoint()
            self.update()

    # Shows the right click menu
    def contextMenuEvent(self, event):
        self.addEventLabels = []
        box_id = None
        videobox = [1]
        if len(videobox) > 0:
            if event.reason() == QtGui.QContextMenuEvent.Mouse:
                posX = event.pos().x()
                posY = event.pos().y()

                index = -1
                for i in range(11):
                    x, y, w, h = 20, 20, 100, 100
                    if posX > x and posX < (x+w) and posY > y and posY < (y+h):
                        index = i

                # If the mouse click is inside a box
                if index != -1:
                    self.context_menu = True
                    menu = QtWidgets.QMenu(self)

                    # for i in videoGlobals.classLabels:
                    #     self.buttonLabels.append(menu.addAction(i))

                    menu.addSeparator()
                    addEvent = menu.addMenu('Add Event')
                    stopEvent = menu.addMenu('Stop Event')
                    delete = menu.addMenu('Delete')
                    deleteBox = delete.addAction('Delete Box')
                    deleteAllBoxes = delete.addAction('Delete All Boxes')
                    deleteFrom = delete.addAction('Delete From Here')
                    deleteTo = delete.addAction('Delete To')
                    stopEvent.setEnabled(False)
                    self.stopEventLabels = []
                    self.checkStopEventMenu = []

                    # # Initiate add Event menu
                    # for label in videoGlobals.highLabels:
                    #     self.addEventLabels.append(addEvent.addAction(label))
                    # changeId = menu.addAction('Change Id')

                    # # Show only annotated high classes of the box
                    # if len(player.videobox[frameCounter].annotation) > 0:
                    #     for annot in player.videobox[frameCounter].annotation[index]:
                    #         if annot in videoGlobals.highLabels and annot not in self.checkStopEventMenu:
                    #             self.checkStopEventMenu.append(annot)
                    #             self.stopEventLabels.append(
                    #                 stopEvent.addAction(annot))
                    #             stopEvent.setEnabled(True)
                    action = menu.exec_(self.mapToGlobal(event.pos()))

                self.buttonLabels = []
                self.context_menu = False
