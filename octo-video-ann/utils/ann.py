class AnnLabels:
    boxid_lbl = "BoxId"
    stframe_lbl = "StartF"
    endframe_lbl = "EndF"
    issue_lbl = "Issue"
    comments_lbl = "Comments"
    metadata_lbl = "Metadata"

    col_names = {boxid_lbl: 0,
                 stframe_lbl: 1,
                 endframe_lbl: 2,
                 issue_lbl: 3,
                 comments_lbl: 4,
                 metadata_lbl: 5
                 }

    issue_names = {"Head": ["Partial-Face",
                            "Extra-Face",
                            "Partial-Hair",
                            "Extra-Hair",
                            "Flicker"
                            ],
                   "Torso": ["Partial-Torso",
                             "Extra-Torso",
                             "Flicker"
                             ],
                   "Arms": ["Partial-Hand",
                            "Extra-Hand",
                            "Partial-Arm",
                            "Extra-Arm",
                            "Partial-Object-Held",
                            "Extra-Object-Held",
                            "Flicker"
                            ],
                   "Legs": ["Partial-Legs",
                            "Extra-Legs",
                            "Flicker"],
                   "Accessories": ["Partial-Headphones",
                                   "Extra-Headphones",
                                   "Partial-Earrings",
                                   "Extra-Earrings",
                                   "Partial-Clothing",
                                   "Extra-Clothing"
                                   ],
                   "Background": ["Extra-Background",
                                  "Secondary-Person(s)"],
                   "Other": ["Type Other Issue"]
                   }


class videoGlobals:

    classLabels = []
    highLabels = []
    annotationColors = ['#00FF00', '#FF00FF', '#FFFF00',
                        '#00FFFF', '#FFA500', '#C0C0C0', '#000000', '#EAEAEA']
    eventColors = ['#9fbf1f', '#087649', '#0a5b75', '#181a8d',
                   '#7969b0', '#76a9ea', '#bef36e', '#edfa84', '#f18ed2', '#753e20']
