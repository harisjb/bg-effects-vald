import time
from .ann import AnnLabels
# Holds the bound box parameters


class BoundBox():

    def __init__(self):
        # self.timestamp = time
        self.bboxes = {}
        self.ann = {}

    def make_boxid(self):
        valid_boxnum = -1
        for i in range(1, len(self.bboxes) + 1):
            if i not in self.bboxes:
                valid_boxnum = i
                return valid_boxnum
        valid_boxnum = len(self.bboxes) + 1
        return valid_boxnum

    def addBox(self, qt_rect):
        boxid = None
        x, y, w, h = qt_rect.getRect()
        if abs(w * h) > 10:
            boxid = self.make_boxid()
            self.bboxes[boxid] = {}
            self.bboxes[boxid][AnnLabels.boxid_lbl] = boxid
            self.bboxes[boxid][AnnLabels.metadata_lbl] = qt_rect
        return boxid

    def add_stframe(self, boxid, frameid):
        self.bboxes[boxid][AnnLabels.stframe_lbl] = frameid

    def add_endframe(self, boxid, frameid):
        self.bboxes[boxid][AnnLabels.endframe_lbl] = frameid

    def removeAllBox(self):
        self.bboxes = {}

    def removeSpecBox(self, boxid):
        removed_id = self.bboxes.pop(boxid)
        if removed_id == None:
            print('Box - {} not found'.format(boxid))
