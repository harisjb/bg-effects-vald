import sys
import os
import cv2
from PyQt5 import QtCore
from PyQt5 import QtCore, QtGui, QtWidgets
from utils.ann import AnnLabels
import csv
from utils.canvas import Canvas


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self):
        super().__init__()
        self.InitWindow()

        # TABLE
        self.create_table()

        # VIDEO PLAYER
        self.create_video_player()

        # self.videoPlayer = QLabel()
        self.videoPlayer = Canvas(self)
        self.videoLayout = QtWidgets.QVBoxLayout()

        # self.videoLayout.SetFixedSize(600, 400)
        self.videoLayout.addWidget(self.videoPlayer)
        self.videoLayout.addLayout(self.controlLayout)

        # VIDEO-TABLE LAYOUT
        plotBox = QtWidgets.QHBoxLayout()
        plotBox.addLayout(self.videoLayout)
        plotBox.addLayout(self.tableLayout)
        plotBox.setSpacing(3)

        # MAIN WINDOW
        mainWidget = QtWidgets.QWidget(self)
        self.setCentralWidget(mainWidget)
        mainWidget.setLayout(plotBox)

    def InitWindow(self):
        self.setWindowTitle("BG Effects Validation Tool")
        left = 180
        top = 150
        height = 700
        width = 1450
        # # setting  the fixed height of window
        # self.setFixedHeight(height)
        # self.setFixedWidth(width)
        # self.center()
        self.setGeometry(left, top, width, height)
        self.show()

    def center(self):
        qr = self.frameGeometry()
        cp = QtWidgets.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        # self.move(qr.topLeft())

    def create_table(self):
        # TABLE
        self.tableWidget = QtWidgets.QTableWidget()
        # self.tableWidget.cellClicked.connect(self.checkTableFrame)
        # enter_other = self.tableWidget.itemAt(2, 3)

        tableFields = QtWidgets.QHBoxLayout()

        self.importButton = QtWidgets.QPushButton("Import")
        self.importButton.clicked.connect(self.importAnn)

        self.exportButton = QtWidgets.QPushButton("Export")
        self.exportButton.clicked.connect(self.exportAnn)

        tableFields.addWidget(self.importButton)
        tableFields.addWidget(self.exportButton)
        # tableFields.addWidget(self.iLabel)
        self.insertBaseRow()

        self.tableLayout = QtWidgets.QVBoxLayout()
        self.tableLayout.addWidget(self.tableWidget)
        self.tableLayout.addLayout(tableFields)

    def create_video_player(self):
        self.filename = None
        # video controls
        controlLayout = QtWidgets.QHBoxLayout()

        # Open
        openButton = QtWidgets.QPushButton("Open...")
        openButton.clicked.connect(self.openFile)
        controlLayout.addWidget(openButton)

        # Play
        self.playButton = QtWidgets.QPushButton()
        self.playButton.setEnabled(False)
        self.playButton.setIcon(self.style().standardIcon(
            QtWidgets.QStyle.SP_MediaPlay))
        self.playButton.clicked.connect(self.play_pause)
        controlLayout.addWidget(self.playButton)

        # Replay
        self.replayButtion = QtWidgets.QPushButton()
        self.replayButtion.setEnabled(False)
        self.replayButtion.setIcon(self.style().standardIcon(
            QtWidgets.QStyle.SP_BrowserReload))
        self.replayButtion.clicked.connect(self.replay)
        controlLayout.addWidget(self.replayButtion)

        # MoveTo Previous Frame
        self.prevF = QtWidgets.QPushButton()
        self.prevF.setEnabled(False)
        self.prevF.setIcon(self.style().standardIcon(
            QtWidgets.QStyle.SP_ArrowBack))
        self.prevF.clicked.connect(self.handlePrevF)
        controlLayout.addWidget(self.prevF)

        # MoveTo Next Frame
        self.nextF = QtWidgets.QPushButton()
        self.nextF.setEnabled(False)
        self.nextF.setIcon(self.style().standardIcon(
            QtWidgets.QStyle.SP_ArrowForward))
        self.nextF.clicked.connect(self.handleNextF)
        controlLayout.addWidget(self.nextF)

        # Frame Num Display
        self.frameNum = QtWidgets.QLabel('Frame --')
        self.frameNum.setFixedWidth(85)
        self.frameNum.setUpdatesEnabled(True)
        self.frameNum.setEnabled(False)
        controlLayout.addWidget(self.frameNum)

        # Slider
        self.positionSlider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.positionSlider.sliderMoved.connect(self.setFPosition)
        self.positionSlider.sliderMoved.connect(self.handleFNum)
        self.positionSlider.sliderPressed.connect(self.setFPosition)
        self.positionSlider.sliderPressed.connect(self.handleFNum)

        self.positionSlider.setSingleStep(1)
        self.positionSlider.setPageStep(20)
        self.positionSlider.setEnabled(False)
        self.positionSlider.setAttribute(
            QtCore.Qt.WA_TranslucentBackground, True)
        controlLayout.addWidget(self.positionSlider)
        self.controlLayout = controlLayout

    def style_choice(self, text):
        self.dropDownName = text
        QtWidgets.QApplication.setStyle(QtWidgets.QStyleFactory.create(text))

    def enablesAfterOpen(self):
        self.playButton.setEnabled(True)
        self.replayButtion.setEnabled(True)
        self.positionSlider.setEnabled(True)
        self.frameNum.setEnabled(True)
        self.prevF.setEnabled(True)
        self.nextF.setEnabled(True)

    def openFile(self):
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(
            self, "Open Movie", QtCore.QDir.homePath())
        # self.cap = None
        # fileName = "/Users/harisjabra/Desktop/Chop/Datasets/Pexels/Set1/Pexels-Videos-2795745.mp4"
        # fileName = "/Users/harisjabra/Desktop/Chop/Datasets/InOfficePersonSeg/RawCapture/Session1_Oct10_21/converted/JC_PS_1.yuv_1920x1080.mp4"
        if fileName != '':
            self.filename = fileName
            self.cap = cv2.VideoCapture(self.filename)
            self.tot_frames = self.cap.get(cv2.CAP_PROP_FRAME_COUNT)
            fps = int(self.cap.get(cv2.CAP_PROP_FPS))
            self.fps_ms = int(1000.0 / fps)
            self.is_playing = False
            self.positionSlider.setRange(1, self.tot_frames)
            self.nextFrameSlot()
            self.enablesAfterOpen()
            self.clearTable()
            self.videoPlayer.BoundBoxes.removeAllBox()
            self.refreshDisp()

    def insertBaseRow(self):
        tot_col = len(AnnLabels.col_names)
        self.tableWidget.setColumnCount(tot_col)
        max_rows = 50
        self.tableWidget.setRowCount(max_rows)
        row_pos = 0
        for col_name, col_pos in AnnLabels.col_names.items():
            qcol_name = QtWidgets.QTableWidgetItem(col_name)
            qcol_name.setFlags(QtCore.Qt.ItemIsEnabled)
            self.tableWidget.setItem(row_pos, col_pos, qcol_name)

    def play_pause(self):
        if not self.is_playing:
            self.play_timer = QtCore.QTimer()
            self.play_timer.setTimerType(QtCore.Qt.PreciseTimer)
            self.play_timer.start(self.fps_ms)
            self.play_timer.timeout.connect(self.nextFrameSlot)
            self.is_playing = True
            self.playButton.setIcon(self.style().standardIcon(
                QtWidgets.QStyle.SP_MediaPause))
        else:
            self.play_timer.stop()
            self.is_playing = False
            self.playButton.setIcon(self.style().standardIcon(
                QtWidgets.QStyle.SP_MediaPlay))
            # self.tableWidget.setCurrentCell(2, 3)

    def replay(self):
        self.setFPosition(1)
        # self.is_playing = False
        self.play_pause()

    def drawVideoBoxes(self):
        bboxes = self.videoPlayer.BoundBoxes.bboxes
        if len(bboxes):
            painter = QtGui.QPainter(self.pix)
            # print(rect.normalized())
            for boxid, ann in bboxes.items():
                self.videoPlayer.set_pen_color(painter)
                rect = ann[AnnLabels.metadata_lbl]
                painter.drawRect(rect)
                painter.setPen(QtGui.QColor(255, 255, 255))
                x, y, w, h = self.videoPlayer.getTopLeft(rect)
                painter.drawText(QtCore.QRectF(
                    x+2, y, w, h), QtCore.Qt.AlignLeft, str(boxid))
            painter.end()
        self.videoPlayer.setPixmap(self.pix)
        # self.videoPlayer.update()
        return None

    def nextFrameSlot(self):
        ret, frame = self.cap.read()
        if ret == True:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            img = QtGui.QImage(frame,
                               frame.shape[1],
                               frame.shape[0],
                               QtGui.QImage.Format_RGB888)
            self.pix = QtGui.QPixmap.fromImage(img)
            # self.pix = self.pix.scaled(self.videoPlayer.width(),
            #                            self.videoPlayer.height(),
            #                            QtCore.Qt.KeepAspectRatio,
            #                            QtCore.Qt.SmoothTransformation)
            self.pix = self.pix.scaled(self.videoPlayer.width(),
                                       self.videoPlayer.height())
            self.drawVideoBoxes()
            self.handleFNum()
            self.positionSlider.setSliderPosition(self.getFPosition())

            # self.repaint()

    def isEmptyTable(self):
        for i in range(1, self.tableWidget.rowCount()):
            for j in range(self.tableWidget.columnCount()):
                if self.tableWidget.item(i, j) is not None:
                    return False
        return True

    def getAnnOverwritePerm(self, disp_msg):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Warning)
        msg.setText(disp_msg)
        msg.setStandardButtons(QtWidgets.QMessageBox.Ok |
                               QtWidgets.QMessageBox.Cancel)
        retval = msg.exec_()
        retval = (retval == QtWidgets.QMessageBox.Ok)
        return retval

    def exportAnn(self):
        if self.filename and not self.isEmptyTable():
            file_ext = '.' + self.filename.split('.')[-1]
            ann_ext = '.csv'
            self.ann_name = self.filename.replace(file_ext, ann_ext)
            overwrite_ok = True
            if os.path.exists(self.ann_name):
                disp_msg = "{} File Exists. Overwrite?".format(self.ann_name)
                overwrite_ok = self.getAnnOverwritePerm(disp_msg)
            # path, _ = QFileDialog.getSaveFileName(self, 'Save File', QDir.homePath() + "/"+self.fName+".csv", "CSV Files(*.csv *.txt)")
            if overwrite_ok:
                with open(self.ann_name, 'w') as stream:
                    print("saving", self.ann_name)
                    writer = csv.writer(stream)
                    delimeter = ','
                    empty_entry = "_"
                    writer = csv.writer(stream, delimiter=delimeter)
                    num_rows, num_cols = self.tableWidget.rowCount(), self.tableWidget.columnCount()
                    if num_rows < 1 and num_cols < 1:
                        raise ValueError("Table was not created")
                    # write header
                    rowdata = []
                    for col in range(num_cols):
                        item = self.tableWidget.item(0, col)
                        rowdata.append(item.text())
                    writer.writerow(rowdata)
                    # write row data
                    for row in range(1, num_rows):
                        boxid = self.tableWidget.item(row, 0)
                        # print(boxid, type(boxid))
                        if boxid is not None:
                            rowdata = []
                            for col in range(num_cols):
                                item = self.tableWidget.item(row, col)
                                if item != None and item != "":
                                    rowdata.append(item.text())
                                else:
                                    rowdata.append(empty_entry)
                            writer.writerow(rowdata)
            else:
                print(
                    'Please take a back-up and rename the file {}'.format(self.ann_name))
        else:
            print("Nothing to Export")

    def clearTable(self):
        self.tableWidget.clearContents()
        self.insertBaseRow()

    def importAnn(self):
        overwrite_ok = True
        if not self.isEmptyTable():
            disp_msg = "Overwrite Existing Ann"
            overwrite_ok = self.getAnnOverwritePerm(disp_msg)
        if overwrite_ok:
            self.clearTable()
            path, _ = QtWidgets.QFileDialog.getOpenFileName(
                self, 'Save File', QtCore.QDir.homePath(), "CSV Files(*.csv *.txt)")
            if path:
                with open(path, 'r') as stream:
                    print("loading", path)
                    reader = csv.reader(stream)
                    for i, row in enumerate(reader):
                        for j in range(len(row)):
                            content = QtWidgets.QTableWidgetItem(row[j])
                            self.tableWidget.setItem(i, j, content)

    def refreshDisp(self):
        self.setFPosition(self.getFPosition())

    def setFPosition(self, frame_num=None):
        if frame_num is None:
            frame_num = self.getFPosition()
        self.cap.set(cv2.CAP_PROP_POS_FRAMES, max(0, frame_num-1))
        self.nextFrameSlot()

    def getFPosition(self):
        return int(self.cap.get(cv2.CAP_PROP_POS_FRAMES))

    def handleFNum(self):
        self.frameNum.setText("Frame %d" % self.getFPosition())

    def handleNextF(self):
        self.nextFrameSlot()

    def handlePrevF(self):
        curr_fpos = self.getFPosition()
        self.setFPosition(curr_fpos - 1)


if __name__ == '__main__':
    App = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    sys.exit(App.exec())
