import tkinter as tk
import cv2
from PIL import Image, ImageTk, ImageEnhance
import PIL
import time
from tkinter import filedialog
import numpy as np


class App:
    def __init__(self, video_source=3):
        self.overlay_img = None
        self.painting = None
        white = (255, 255, 255)
        x1, y1 = [0, 0]
        x2, y2 = [0, 0]
        self.appName = "Kamera"
        self.window = tk.Tk()
        self.window.title(self.appName)
        self.window.geometry("640x520+0+0")
        self.window.resizable(0, 0)
        self.window.attributes('-alpha', 0.5)
        self.window['bg'] = 'black'
        self.video_source = video_source
        self.vid = MyVideoCapture(self.video_source)

        self.canvas = tk.Canvas(
            self.window, width=self.vid.width, height=self.vid.height, bg='red')
        self.canvas.pack()
        self.canvas.bind("<B1-Motion>", self.paint)

        self.update()
        self.window.mainloop()

    def paint(self, event):
        white = (255, 255, 255)
        x1, y1 = (event.x - 2), (event.y - 2)
        x2, y2 = (event.x + 2), (event.y + 2)
        self.canvas.create_oval(x1, y1, x2, y2, fill="white", outline="white")

    def update(self):
        isTrue, frame = self.vid.getFrame()

        # Convert frame to TK and put on canvas
        if isTrue:
            self.photo = ImageTk.PhotoImage(image=PIL.Image.fromarray(frame))
            self.canvas.create_image(0, 0, image=self.photo, anchor=tk.NW)

        self.window.after(10, self.update)


class MyVideoCapture:
    def __init__(self, video_source=3):
        self.vid = cv2.VideoCapture(video_source)
        if not self.vid.isOpened():
            raise ValueError(
                "Unable to open this camera \n select another video source", video_source)

        self.width = self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)

        self.flipped = True

    def getFrame(self):
        if self.vid.isOpened():
            isTrue, frame = self.vid.read()
            if isTrue and self.flipped:
                frame = cv2.flip(frame, 1)
            if isTrue:
                return (isTrue, cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
            else:
                return (isTrue, None)
        else:
            return (isTrue, None)

    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()


if __name__ == "__main__":
    App()
