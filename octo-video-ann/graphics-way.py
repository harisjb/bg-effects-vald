from PyQt5.QtCore import QPointF, Qt
from PyQt5.QtWidgets import QGraphicsRectItem, QGraphicsScene, QGraphicsView, QApplication
from PyQt5.QtGui import QBrush, QPainter, QPen, QPixmap, QPolygonF
import sys

# app = QApplication(sys.argv)

# scene = QGraphicsScene(0, 0, 400, 200)

# rectitem = QGraphicsRectItem(0, 0, 360, 20)
# rectitem.setPos(20, 20)
# rectitem.setBrush(QBrush(Qt.red))
# rectitem.setPen(QPen(Qt.cyan))
# scene.addItem(rectitem)

# textitem = scene.addText("QGraphics is fun!")
# textitem.setPos(100, 100)

# pixmap = QPixmap(
#     "/Users/harisjabra/Desktop/Chop/Datasets/Pexels/Training/Set1/pexels-alena-darmel-7184326_ds=0.5/F00001.rgb.png")
# pixmapitem = scene.addPixmap(pixmap)
# pixmapitem.setPos(250, 70)

# scene.addPolygon(
#     QPolygonF(
#         [
#             QPointF(30, 60),
#             QPointF(270, 40),
#             QPointF(400, 200),
#             QPointF(20, 150),
#         ]),
#     QPen(Qt.darkGreen),
# )

# view = QGraphicsView(scene)
# view.setRenderHint(QPainter.Antialiasing)
# view.show()

# app.exec_()


import sys
import os
from os import path
import functools
from argparse import ArgumentParser

import cv2
from PIL import Image
import numpy as np
import torch
from collections import deque

from PyQt5.QtWidgets import (QMainWindow, QWidget,      QApplication, QComboBox,
                             QHBoxLayout, QLabel, QPushButton, QTextEdit,
                             QPlainTextEdit, QVBoxLayout, QSizePolicy, QButtonGroup, QSlider,
                             QShortcut, QRadioButton, QProgressBar, QFileDialog)

from PyQt5.QtGui import QPixmap, QKeySequence, QImage, QTextCursor
from PyQt5.QtCore import Qt, QTimer
from PyQt5 import QtCore
from PyQt5 import QtCore, QtGui, QtWidgets


class GraphicsVideoPlayer():
    def __init__(self):
        pass


class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()
        self.InitWindow()

        # TABLE
        self.create_table()

        # VIDEO PLAYER
        # self.create_video_player()

        # self.videoPlayer = QLabel()
        self.videoPlayer = GraphicsVideoPlayer()
        self.videoLayout = QVBoxLayout()
        self.videoLayout.addWidget(self.videoPlayer)
        # self.videoLayout.addLayout(self.controlLayout)

        # VIDEO-TABLE LAYOUT
        plotBox = QHBoxLayout()
        plotBox.addLayout(self.videoLayout)
        plotBox.addLayout(self.tableLayout)

        # MAIN WINDOW
        mainWidget = QWidget(self)
        self.setCentralWidget(mainWidget)
        mainWidget.setLayout(plotBox)

    def InitWindow(self):
        self.setWindowTitle("Mamba Validation Tool")
        # self.setWindowIcon(QtGui.QIcon(iconName))
        # self.setWindowState(QtCore.Qt.WindowMaximized)
        self.center()
        self.show()

    def center(self):
        qr = self.frameGeometry()
        cp = QtWidgets.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        # self.move(qr.topLeft())

    def create_table(self):
        # TABLE
        self.tableWidget = QtWidgets.QTableWidget()
        self.tableWidget.cellClicked.connect(self.checkTableFrame)
        tableFields = QHBoxLayout()

        self.startTime = QtWidgets.QLineEdit()
        self.startTime.setPlaceholderText("Select Start Time")

        self.endTime = QtWidgets.QLineEdit()
        self.endTime.setPlaceholderText("Select End Time")

        self.iLabel = QComboBox(self)
        self.iLabel.addItem("1. Face")
        self.iLabel.addItem("2. Hands")
        self.iLabel.addItem("3. Torso")
        self.iLabel.addItem("4. Legs")
        self.iLabel.addItem("5. Objects-Held")
        self.iLabel.addItem("6. Objects-False-Positive")
        self.iLabel.activated[str].connect(self.style_choice)
        tableFields.addWidget(self.startTime)
        tableFields.addWidget(self.endTime)
        tableFields.addWidget(self.iLabel)
        self.insertBaseRow()

        self.tableLayout = QVBoxLayout()
        self.tableLayout.addWidget(self.tableWidget)
        self.tableLayout.addLayout(tableFields)

    def create_video_player(self):
        self.filename = None
        # video controls
        controlLayout = QHBoxLayout()
        # Open
        openButton = QPushButton("Open...")
        openButton.clicked.connect(self.openFile)
        controlLayout.addWidget(openButton)
        # Play
        self.playButton = QPushButton()
        self.playButton.setEnabled(False)
        self.playButton.setIcon(self.style().standardIcon(
            QtWidgets.QStyle.SP_MediaPlay))
        self.playButton.clicked.connect(self.play)
        controlLayout.addWidget(self.playButton)
        # Pause
        self.pauseButton = QPushButton()
        self.pauseButton.setEnabled(False)
        self.pauseButton.setIcon(self.style().standardIcon(
            QtWidgets.QStyle.SP_MediaPause))
        self.pauseButton.clicked.connect(self.pause)
        controlLayout.addWidget(self.pauseButton)
        # Label
        self.lbl = QLabel('00:00:00')
        self.lbl.setFixedWidth(60)
        self.lbl.setUpdatesEnabled(True)
        controlLayout.addWidget(self.lbl)
        # Slider
        self.positionSlider = QSlider(Qt.Horizontal)
        self.positionSlider.setRange(0, 100)
        self.positionSlider.sliderMoved.connect(self.setPosition)
        self.positionSlider.sliderMoved.connect(self.handleLabel)
        self.positionSlider.setSingleStep(2)
        self.positionSlider.setPageStep(20)
        self.positionSlider.setAttribute(Qt.WA_TranslucentBackground, True)
        controlLayout.addWidget(self.positionSlider)
        self.controlLayout = controlLayout

    def style_choice(self, text):
        self.dropDownName = text
        QApplication.setStyle(QtWidgets.QStyleFactory.create(text))

    def openFile(self):
        # fileName, _ = QFileDialog.getOpenFileName(
        #     self, "Open Movie", QtCore.QDir.homePath())
        # self.cap = None
        fileName = "/Users/harisjabra/Desktop/Chop/Datasets/Pexels/Set1/Pexels-Videos-2795745.mp4"
        if fileName != '':
            self.filename = fileName
            self.playButton.setEnabled(True)
            self.pauseButton.setEnabled(True)
            self.cap = cv2.VideoCapture(self.filename)
            self.nextFrameSlot()

    def insertBaseRow(self):
        self.tableWidget.setColumnCount(4)  # , Start Time, End Time, TimeStamp
        self.tableWidget.setRowCount(50)
        self.rowNo = 1
        self.colNo = 0
        self.tableWidget.setItem(
            0, 0, QtWidgets.QTableWidgetItem("Start Time"))
        self.tableWidget.setItem(0, 1, QtWidgets.QTableWidgetItem("End Time"))
        self.tableWidget.setItem(0, 2, QtWidgets.QTableWidgetItem("ROI Index"))
        self.tableWidget.setItem(
            0, 3, QtWidgets.QTableWidgetItem("Issue Name"))

    def checkTableFrame(self, row, column):
        if ((row > 0) and (column < 2)):
            # print("Row %d and Column %d was clicked" % (row, column))
            item = self.tableWidget.item(row, column)
            if (item != (None and "")):
                try:
                    itemFrame = item.text()
                    itemFrame = itemFrame.split(":")
                    frameTime = int(
                        itemFrame[2]) + int(itemFrame[1])*60 + int(itemFrame[0])*3600
                except:
                    print('Error writing table frame')

    def paintEvent(self, event):

        # painter = QtGui.QPainter(self)
        rectPainter = QtGui.QPainter(self)
        # boxIdPainter = QtGui.QPainter()

        rectPainter.drawRect(10, 10, 100, 100)
        rectPainter.end()

        # if self.moved and not event.rect().size().__eq__(self.surface.surfaceFormat().sizeHint()):
        # if not rectPainter.isActive():
        #     rectPainter.begin(self)

    def play(self):
        if self.filename is not None:
            if self.cap is None:
                self.cap = cv2.VideoCapture(self.filename)
            fps = int(self.cap.get(cv2.CAP_PROP_FPS))
            self.timer = QTimer()
            millisecs = int(1000.0 / fps)
            self.timer.setTimerType(Qt.PreciseTimer)
            self.timer.timeout.connect(self.nextFrameSlot)
            self.timer.start(millisecs)

    def pause(self):
        if self.filename is not None:
            self.timer.stop()

    def nextFrameSlot(self):
        ret, frame = self.cap.read()
        # frame = cv2.resize(frame, ())
        # ph, pw = self.videoPlayer.height(), self.videoPlayer.width()
        # frame = cv2.resize(frame, (1280, 720))
        if ret == True:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            img = QImage(frame, frame.shape[1],
                         frame.shape[0], QImage.Format_RGB888)
            self.pix = QPixmap.fromImage(img)
            # pix = self.videoPlayer.pixmap
            self.pix = self.pix.scaled(self.videoPlayer.width(), self.videoPlayer.height(
            ), Qt.KeepAspectRatio, Qt.SmoothTransformation)
            self.videoPlayer.setPixmap(self.pix)
            self.repaint()

    def setPosition(self, position):
        pass

    def handleLabel(self):
        pass


if __name__ == '__main__':
    App = QApplication(sys.argv)
    window = MainWindow()
    # window = SimpleApp()
    window.show()
    sys.exit(App.exec())
