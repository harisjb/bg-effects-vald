import os
from PyQt5 import QtCore, QtGui, QtWidgets, QtMultimedia, QtMultimediaWidgets
from PyQt5.QtCore import Qt, QPoint, QRect
from PyQt5.QtGui import QPixmap, QPainter


class Widget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(Widget, self).__init__(parent)

        self._scene = QtWidgets.QGraphicsScene(self)
        self._gv = QtWidgets.QGraphicsView(self._scene)

        self._videoitem = QtMultimediaWidgets.QGraphicsVideoItem()
        self._scene.addItem(self._videoitem)
        self._player = QtMultimedia.QMediaPlayer(
            self, QtMultimedia.QMediaPlayer.VideoSurface)
        self._player.stateChanged.connect(self.on_stateChanged)

        # file = os.path.join(os.path.dirname(__file__), "small.mp4")
        file = "/Users/harisjabra/Desktop/Chop/Datasets/Pexels/Set2/pexels-anastasia-shuraeva-8510108.mp4"
        self._player.setMedia(QtMultimedia.QMediaContent(
            QtCore.QUrl.fromLocalFile(file)))
        button = QtWidgets.QPushButton("Play")
        button.clicked.connect(self._player.play)

        self.begin, self.dest = QPoint(), QPoint()
        self.resize(640, 480)
        lay = QtWidgets.QVBoxLayout(self)

        self.pix = QPixmap(self.rect().size())
        self.pix.fill(Qt.white)
        self.begin, self.dest = QPoint(), QPoint()
        lay.addWidget(self._gv)
        lay.addWidget(button)

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.drawPixmap(QPoint(), self.pix)

        if not self.begin.isNull() and not self.dest.isNull():
            rect = QRect(self.begin, self.dest)
            painter.drawRect(rect.normalized())

    def mousePressEvent(self, event):
        if event.buttons() & Qt.LeftButton:
            self.begin = event.pos()
            self.dest = self.begin
            self.update()

    def mouseMoveEvent(self, event):
        if event.buttons() & Qt.LeftButton:
            self.dest = event.pos()
            self.update()

    def mouseReleaseEvent(self, event):
        if event.button() & Qt.LeftButton:
            rect = QRect(self.begin, self.dest)
            painter = QPainter(self.pix)
            print(rect.normalized())
            painter.drawRect(rect.normalized())

            self.begin, self.dest = QPoint(), QPoint()
            self.update()

    @QtCore.pyqtSlot(QtMultimedia.QMediaPlayer.State)
    def on_stateChanged(self, state):
        self._player.setVideoOutput(self._videoitem)
        if state == QtMultimedia.QMediaPlayer.PlayingState:
            self._ellipse_item = QtWidgets.QGraphicsEllipseItem(
                QtCore.QRectF(50, 50, 40, 40), self._videoitem)
            self._ellipse_item.setBrush(QtGui.QBrush(QtCore.Qt.green))
            self._ellipse_item.setPen(QtGui.QPen(QtCore.Qt.red))
            self._gv.fitInView(self._videoitem, QtCore.Qt.KeepAspectRatio)

    # @QtCore.pyqtSlot(QtMultimedia.QMediaPlayer.State)
    # def on_stateChanged(self, state):
    #     # self._player.setVideoOutput(self._videoitem)
    #     if state == QtMultimedia.QMediaPlayer.PlayingState:
    #         self._player.state == QtMultimedia.QMediaPlayer.pause


if __name__ == '__main__':
    import sys
    app = QtWidgets.QApplication(sys.argv)
    w = Widget()
    w.show()
    sys.exit(app.exec_())
