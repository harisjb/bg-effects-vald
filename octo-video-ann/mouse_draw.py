import os
import sys
from PyQt5.QtWidgets import (QApplication, QWidget, QHBoxLayout,
                             QVBoxLayout)
# from PyQt5 import QtCore, QtGui, QtWidgets, QtMultimedia, QtMultimediaWidgets
from PyQt5.QtCore import Qt, QPoint, QRect
from PyQt5.QtGui import QPixmap, QPainter


class MyApp(QWidget):
    def __init__(self):
        super().__init__()
        self.window_width, self.window_height = 1200, 800
        self.setMinimumSize(self.window_width, self.window_height)

        layout = QVBoxLayout()
        self.setLayout(layout)

        self.pix = QPixmap(self.rect().size())
        self.pix.fill(Qt.white)
        self.begin, self.dest = QPoint(), QPoint()

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.drawPixmap(QPoint(), self.pix)

        if not self.begin.isNull() and not self.dest.isNull():
            rect = QRect(self.begin, self.dest)
            painter.drawRect(rect.normalized())

    def mousePressEvent(self, event):
        if event.buttons() & Qt.LeftButton:
            self.begin = event.pos()
            self.dest = self.begin
            self.update()

    def mouseMoveEvent(self, event):
        if event.buttons() & Qt.LeftButton:
            self.dest = event.pos()
            self.update()

    def mouseReleaseEvent(self, event):
        if event.button() & Qt.LeftButton:
            rect = QRect(self.begin, self.dest)
            painter = QPainter(self.pix)
            print(rect.normalized())
            painter.drawRect(rect.normalized())

            self.begin, self.dest = QPoint(), QPoint()
            self.update()


app = QApplication(sys.argv)
myApp = MyApp()
myApp.show()
sys.exit(app.exec_())

# if __name__ == '__main__':
#     app = QApplication(sys.argv)
#     # app.setStyleSheet()
#     myApp = MyApp()
#     myApp.show()
